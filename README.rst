Inouk Odoo PostgreSQL Health Check
===================================

An addon that check PostgreSQL Replication Status for muppy.io pack8s. This addon 
just return the PostgreSQL Replication Status when calling endpoint '/inouk_pg_health_check'


Installation
============

To install this module, you need to:

Download the module and add it to your Odoo addons folder. Afterward, just call 
the endpoint '/inouk_pg_health_check' in order to get the PostgreSQL Replication Status.


Bugs
============
Please report bugs to https://gitlab.com/inouk/inouk_odoo_pg_health_check


Author(s)
============

* Cyril MORISSE <cmorisse@boxes3.net>
* Mathias MANGON <mangon.m@gmail.com>