{
    "name": "Inouk Odoo PostgreSQL Health Check",
    "summary": """Odoo addon that check PostgreSQL Replication Status for muppy.io pack8s""",
    "version": "14.0.1.0.0",
    "category": "Extra Tools",
    "license": "LGPL-3",
    "website": "https://gitlab.com/inouk/inouk_odoo_pg_health_check.git",
    "author": "Cyril MORISSE, Mathias MANGON",
    "contributors": ["Cyril MORISSE <cmorisse@boxes3.net>", "Mathias MANGON <mangon.m@gmail.com>", ],
    "depends": ["base"],
    "images": [],
    "application": False,
    "installable": True,
}
